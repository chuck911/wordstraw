const fs = require('fs-extra')
const Koa = require('koa')
const Router = require('koa-router')
const koaNunjucks = require('koa-nunjucks-2')
const path = require('path')
const koaBody = require('koa-body')
const chardet = require('chardet')
const iconv = require('iconv-lite')
const low = require('lowdb')
const session = require('koa-session')
const FileSync = require('lowdb/adapters/FileSync')
const uniqid = require('uniqid')
const Queue = require('bull')
const dayjs = require('dayjs')

const app = new Koa()
const router = new Router()

const db = low(new FileSync('./data/db.json')).defaults({presets:[]})

const queue = new Queue('baidu-keywords')
let wipJobs = {}
let progress = 0
queue.on('global:progress', (_id, _progress) => {
	progress = _progress
})
queue.on('global:completed', (_id, id) => {
	id = id.slice(1,-1)	
	delete wipJobs[id]
})

app.use(session({ maxAge: 86400000*30 }, app))
	.use(async (ctx, next) => {
		if (ctx.path !== '/login' && !ctx.session.user) {
			ctx.redirect('/login')
		}
		await next()
	})
app.keys = ['wHosy0urDAddY?']

app.use(koaNunjucks({
	ext: 'njk',
	path: path.join(__dirname, 'views'),
	nunjucksConfig: {}
}))

router.get('/login', async ctx => {
	await ctx.render('login')
})

router.post('/login', koaBody(), async ctx => {
	const postData = ctx.request.body
	if (postData.password == '1QA2WS') {
		ctx.session.user = 'admin'
		ctx.redirect('/')
	} else {
		ctx.redirect('/login')
	}
})

router.get('/logout', async ctx => {
	ctx.session = null
	ctx.redirect('/login')
})

router.get('/', async ctx => {
	const presets = db.get('presets').value()
	await ctx.render('list', {presets})
})

router.get('/new', async ctx => {
	await ctx.render('new')
})

router.post('/new', koaBody({multipart:true}), async ctx => {
	const file = ctx.request.files.file
	let text
	if (file.size>0) {
		const buffer = fs.readFileSync(file.path)
		const encoding = chardet.detect(buffer).toLowerCase()
		text = iconv.decode(buffer, encoding=='utf-8'?'utf-8':'GBK')
	} else {
		text = ctx.request.body.text
	}
	const prefixes = text.split(/\r?\n/)
	const id = uniqid.time()
	db.get('presets').push({
		id,
		sample5: prefixes.slice(0,5).join(','),
		sample50: prefixes.slice(0,50).join(','),
		amount: prefixes.length
	}).write()
	fs.writeFileSync(`./data/text/${id}.txt`, prefixes.join('\r\n'))
	// ctx.body = prefixes
	ctx.redirect('/preset/'+id)
})

router.get('/preset/:id', async ctx => {
	const id = ctx.params.id
	const preset = db.get('presets').find({id}).value()
	if (!preset) ctx.status = 404
	const resultPath = './data/words/'+id
	let files = []
	if (fs.existsSync(resultPath)) {
		files = fs.readdirSync(resultPath).reverse()
	}
	files = files.map(file => {
		const ts = file.replace('.txt', '')
		const day = dayjs(new Date(ts*1000))
		const date = day.format('YYYY-MM-DD HH:mm:ss')
		return {date, path:file}
	})
	const wip = wipJobs[id]
	await ctx.render('preset', {preset, files, progress, wip})
})

router.get('/preset-file/:id', async ctx => {
	const id = ctx.params.id
	ctx.body = fs.readFileSync(`./data/text/${id}.txt`, { encoding: 'utf8' })
})

router.post('/addjob', koaBody(), async ctx => {
	const id = ctx.request.body.id
	db.get('presets').find({id}).set('wip', true).write()
	wipJobs[id] = 1
	wipJobId = id
	await queue.add({id})
	ctx.redirect('/preset/'+id)
})

router.get('/down/:id/:file', async ctx => {
	const {id, file} = ctx.params
	ctx.body = fs.createReadStream(`./data/words/${id}/${file}`)
	ctx.attachment(file)
})

router.get('/down-gb/:id/:file', async ctx => {
	const {id, file} = ctx.params
	ctx.body = fs.createReadStream(`./data/words/${id}/${file}`,{ encoding: 'utf8' }).pipe(iconv.encodeStream('gbk'))
	ctx.attachment(file)
})

router.get('/completed/:id', async ctx => {
	const id = ctx.params.id
	ctx.body = wipJobs[id] ? 0 : 1
})

app.use(router.routes())
	.use(router.allowedMethods())


app.listen(3136)