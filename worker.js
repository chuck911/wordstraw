const Queue = require('bull')
const fs = require('fs-extra')
const got = require('got')
const iconv = require('iconv-lite')
const queue = new Queue('baidu-keywords')

queue.process(async job => {
	const text = fs.readFileSync(`./data/text/${job.data.id}.txt`, 'utf-8')
	const prefixes = text.split(/\r?\n/)
	let i = 0
	let words = []
	for (const prefix of prefixes) {
		try {
			let keywords = await getKeywords(prefix)
			keywords = keywords.filter(word=>!word.match(/肺|疫/))
			words = words.concat(keywords)
			// console.log(words)
		} catch (error) {
			console.error(error)
		}
		job.progress(Math.round(++i/prefixes.length*100))
	}
	const filename = Math.floor(Date.now() / 1000)+'.txt'
	await fs.outputFile(`./data/words/${job.data.id}/${filename}`, words.join('\r\n'))
	return job.data.id
	// console.log(obj)
})

async function getKeywords(prefix) {
	const res = await got('http://suggestion.baidu.com/su?wd='+encodeURIComponent(prefix), {responseType: 'buffer'})
	const str = iconv.decode(res.body, 'gbk')
	let obj = eval('('+str.slice(17,-2)+')')
	return obj.s
}